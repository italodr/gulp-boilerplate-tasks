var fs          = require('fs');
var userConfig  = 'config.json';
var gutil       = require('gulp-util');

if (fs.existsSync(userConfig)) {
    var path = fs.readFileSync(userConfig, 'utf-8');
    path = JSON.parse(path.toString());

} else {
    var fileName     = gutil.colors.gray('\'') + gutil.colors.cyan('config.json') +gutil.colors.gray('\''),
        errorMessage = gutil.colors.red('Configuration file \'config.json\' not found in root') +'\r\n',
        hint         = gutil.colors.gray('Copy the \'config-sample.json\' in your root directory and rename it as \'config.json\'') +'\r\n';

    console.log('');
    gutil.log(fileName, errorMessage + hint);
    process.exit(-1);
}

module.exports = {

    browserSync: {
        proxy: path.browsersync.config.proxy,
        startPath: path.browsersync.config.startPath,
        port: path.browsersync.config.port,
        https: path.browsersync.config.https,
        injectChanges: true,
        codeSync: true,
        files: [
            "stylesheets/*.css",
            // Exclude Map files
            "!stylesheets/**.map"
        ]
    },
    clean: {
        dest: path.dest.base,
        images: path.dest.base + path.dest.images,
        stylesheets: path.dest.base + path.dest.stylesheets,
        javascripts: path.dest.base + path.dest.javascripts,
        markups: path.dest.base,
        data: path.src.base + path.src.data.route,
        output: path.src.data.output
    },
    sass: {
        src: [
            path.src.base + path.src.stylesheets + "/**/*.{sass,scss}"
        ],
        excludes: [
            "!"+ path.src.base + path.src.stylesheets + "/_*",
            "!"+ path.src.base + path.src.stylesheets + "/**/*.map",
            "!"+ path.dest.base + path.dest.stylesheets + "/**/*.map"
        ],
        dest: path.dest.base + path.dest.stylesheets,
        settings: {
            // Required if you want to use SASS syntax
            // See https://github.com/dlmanning/gulp-sass/issues/81
            sourceComments: 'map',
            imagePath: path.src.images // Used by the image-url helper
        }
    },
    images: {
        src: [
            path.src.base + path.src.images + '/**'
        ],
        excludes: [
            path.src.base + path.src.images + '/_*'
        ],
        dest: path.dest.base + path.dest.images
    },
    concat_json: {
        src: [
            path.src.base + path.src.data.route + '/**/-*.yml'
        ],
        excludes: [
            "!"+ path.src.base + path.src.data.route + '/_*',
            "!"+ path.src.base + path.src.data.route + '/**/_*.yml'
        ],
        dest: path.src.base + path.src.data.route,
        output: path.src.data.yml_output
    },
    concat_javascript: {
        src: path.src.concat_js.files,
        excludes: [
            "!"+ path.src.base + path.src.javascripts + '/_*',
            "!"+ path.src.base + path.src.javascripts + '/**/_*.js'
        ],
        dest: path.dest.base + path.dest.javascripts,
        output: path.dest.concat_js.js_output
    },
    copy_javascript: {
        src: path.src.copy_js.files,
        excludes: [
            "!"+ path.src.base + path.src.javascripts + '/_*',
            "!"+ path.src.base + path.src.javascripts + '/**/_*.js'
        ],
        dest: path.dest.base + path.dest.javascripts
    },
    copy_fonts: {
        src: [
            path.src.base + path.src.fonts + '/**/*'
        ],
        excludes: [
            "!"+ path.src.base + path.src.fonts + '/_*',
            "!"+ path.src.base + path.src.fonts + '/**/_*'
        ],
        dest: path.dest.base + path.dest.fonts
    },
    copy_bower: {
        src: path.src.copy_bower.files,
        dest: path.dest.base + path.dest.vendors
    },
    markup: {
        src: [
            path.src.base + path.src.markups + '/**/*.twig'
        ],
        excludes: [
            "!"+ path.src.base + path.src.markups + '/_*',
            "!"+ path.src.base + path.src.markups + '/**/_*.twig'
        ],
        dest: path.dest.base,
        data: path.src.base + path.src.data.route +'/'+ path.src.data.json_output
    }
};
