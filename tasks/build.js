var gulp = require('gulp');

gulp.task('build', ['sass', 'images', 'markup', 'copy:fonts', 'copy:bower', 'concat:javascript', 'copy:javascript']);
