var gulp    = require('gulp');
var concat  = require('gulp-concat');
var uglify  = require('gulp-uglify');
var config  = require('../config').concat_javascript;

gulp.task('concat:javascript', function() {
    var source = config.src.concat(config.excludes);

    return gulp.src(source)
    .pipe(uglify())
    .pipe(concat(config.output, { newLine: ';' }))
    .pipe(gulp.dest(config.dest));
});
