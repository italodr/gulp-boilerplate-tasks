var gulp    = require('gulp');
var gutil   = require('gulp-util');
var yaml    = require('gulp-yaml');
var concat  = require('gulp-concat');
var plumber = require('gulp-plumber');
var config  = require('../config').concat_json;

var throwError = function (err) {
    var fileName     = gutil.colors.gray('\'') + gutil.colors.cyan('Concat:json') +gutil.colors.gray('\''),
        errorMessage = gutil.colors.yellow('Error on yaml concat and parsed to json\n');

    gutil.log(fileName, errorMessage + err);
};

gulp.task('concat:json', function() {
    var source = config.src.concat(config.excludes);

    return gulp.src(source)
        .pipe(plumber({ errorHandler: throwError }))
        .pipe(concat(config.output))
        .pipe(gulp.dest(config.dest))
        .pipe(yaml())
        .pipe(gulp.dest(config.dest));
});
