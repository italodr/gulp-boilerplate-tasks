var gulp   = require('gulp');
var config = require('../config').copy_bower;

/**
 * Copy bower elements
 */
gulp.task('copy:bower', function() {
    return gulp.src(config.src)
        .pipe(gulp.dest(config.dest));
});
