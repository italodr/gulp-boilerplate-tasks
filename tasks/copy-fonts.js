var gulp   = require('gulp');
var config = require('../config').copy_fonts;

/**
 * Copy fonts to folder
 */
gulp.task('copy:fonts', function() {
    var source = config.src.concat(config.excludes);
    return gulp.src(source)
        .pipe(gulp.dest(config.dest));
});
