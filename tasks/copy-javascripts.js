var gulp    = require('gulp');
var uglify  = require('gulp-uglify');
var rename  = require('gulp-rename');
var config  = require('../config').copy_javascript;

gulp.task('copy:javascript', function() {
    var source = config.src.concat(config.excludes);

    return gulp.src(source)
    .pipe(gulp.dest(config.dest))
    .pipe(uglify())
    .pipe(rename(function (path) {
        path.dirname += "/min";
        path.basename += ".min";
    }))
    .pipe(gulp.dest(config.dest));
});
