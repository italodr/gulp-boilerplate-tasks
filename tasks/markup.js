var gulp        = require('gulp');
var gutil       = require('gulp-util');
var browserSync = require('browser-sync');
var twig        = require('gulp-twig');
var fs          = require('fs');
var config      = require('../config').markup;

var throwError = function (err) {
    var fileName     = gutil.colors.gray('\'') + gutil.colors.cyan('Sass') +gutil.colors.gray('\''),
        errorMessage = gutil.colors.yellow('Error on yaml concat and parsed to json');

    gutil.log(fileName, errorMessage);
};

gulp.task('markup', ['concat:json'], function () {
    if (!fs.existsSync(config.data)) {
        throwError();
        return true;
    }
    var json_data = fs.readFileSync(config.data, 'utf-8');
    json_data = JSON.parse(json_data.toString());
    var source = config.src.concat(config.excludes);

    return gulp.src(source)
        .pipe(twig({
            data: json_data,
            cache: false
        }))
        .pipe(gulp.dest(config.dest))
        .pipe(browserSync.reload({stream:true}));
});
