var gulp         = require('gulp');
var gutil        = require('gulp-util');
var browserSync  = require('browser-sync');
var sass         = require('gulp-sass');
var sourcemaps   = require('gulp-sourcemaps');
var handleErrors = require('../util/handleErrors');
var autoprefixer = require('gulp-autoprefixer');
var pixrem       = require('gulp-pixrem');
var plumber      = require('gulp-plumber');
var rename       = require('gulp-rename');
var minify       = require('gulp-minify-css');
var config       = require('../config').sass;

var throwError = function (err) {
    var fileName     = gutil.colors.gray('\'') + gutil.colors.cyan('Sass') +gutil.colors.gray('\''),
        errorMessage = gutil.colors.red('Error compiling sass. '+ err);

    gutil.log(fileName, errorMessage);
};


gulp.task('sass', ['images'], function () {

    browserSync.notify('Compiling Sass');
    var source = config.src.concat(config.excludes);

    return gulp.src(source)
        .pipe(plumber({ errorHandler: throwError }))
        .pipe(sourcemaps.init())
        .pipe(sass(config.settings))
        .on('error', handleErrors)
        .pipe(pixrem())
        .pipe(autoprefixer({ browsers: ['last 2 version'], cascade: false }))
        .pipe(sourcemaps.write('.', { includeContent: false }))
        .pipe(gulp.dest(config.dest))
        .pipe(minify({ keepSpecialComments:0 }))
        .pipe(rename({ suffix:'.min' }))
        .pipe(gulp.dest(config.dest))
        .pipe(browserSync.reload({stream:true}));
});
