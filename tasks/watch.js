var gulp    = require('gulp');
var config  = require('../config');

gulp.task('watch', ['browserSync'], function() {
    gulp.watch(config.sass.src,   ['sass']);
    gulp.watch(config.images.src, ['images']);
    gulp.watch([config.concat_javascript.src, config.copy_javascript.src],
               ['concat:javascript', 'copy:javascript']);
    gulp.watch([config.concat_json.src, config.markup.src],
               ['markup']);
});
